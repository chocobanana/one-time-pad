<?php

function xor_strings() {
    $args = func_get_args();
    $msg = array_shift($args);
    $key = array_shift($args);
    $base = 36;
    $msg = gmp_init($msg, $base);
    $key = gmp_init($key, $base);
    $xor = gmp_xor($msg, $key);
    $xor = gmp_strval($xor, $base);
    if (count($args) > 0) {
        array_unshift($args, $xor);
        return call_user_func_array('xor_strings', $args);
    }
    return $xor;
}

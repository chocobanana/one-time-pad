<?php

/**
 * Usage:
 * 
 * First, generate the keys you are going to use with:
 * $ < /dev/urandom tr -dc a-z0-9 | head -c${1:-144};echo;
 * 
 * Then call the script with arguments $msg $key1 $key2 etc:
 * $ php xor_strings_cli.php hello abcde fghij
 * 
 * To decrypt use the same scripts with the keys in reverse order:
 * $ php xor_strings_cli.php klmn fghij abcde
 */

require __DIR__ . '/src/xor_strings.php';

// Remove first argument, which is the name of the script
array_shift($argv); 

// XOR all the strings (message + keys)
$xor_result = call_user_func_array('xor_strings', $argv);

// Remove first argument, which is the original message
$msg = array_shift($argv);

// So now we have the keys and the XOR result
$parts = $argv;
array_push($parts, $xor_result);

$decrypted = call_user_func_array('xor_strings', $parts);

echo "Message:\n";
echo $msg . "\n\n";

$i = 0;
$count = count($parts);
foreach ($parts as $part) {
    $i++;
    $key_or_xor = $count != $i ? "(key)" : "(XOR result)";
    echo "Part {$i} {$key_or_xor}:\n";
    echo $part . "\n\n";
}

echo "Test:\n";
echo $decrypted . "\n\n";

echo "Whether you are encrypting or decrypting, Part 3 is the result.\n\n";
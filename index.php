<?php

require __DIR__ . '/src/xor_strings.php';

$msg = "hola";
$part1 = "abcd";
$part2 = "efgh";

$part3 = xor_strings($msg, $part1, $part2);
$decrypted = xor_strings($part3, $part2, $part1);

echo "Message:<br>";
echo $msg . "<br><br>";

echo "Part1:<br>";
echo $part1 . "<br><br>";

echo "Part2:<br>";
echo $part2 . "<br><br>";

echo "Part3:<br>";
echo $part3 . "<br><br>";

echo "Decrypted:<br>";
echo $decrypted . "<br><br>";
